function variables(string)
{
    var len=string.length;
    var i=0;
    var final='';
    for(i=0;i<len;i++)
    {
        var str1=string[i].toUpperCase();
        var str2=string[i].toLowerCase();
        if(string[i]===str1)
        {
            final=final+str2;
        }
        else if(string[i]===str2)
        {
            final=final+str1;
        }
    }
    return(final);
}
module.exports=variables;
